import { getLocation, getWeather, getDayOfWeek, getIcon, formatDate } from './providers/weather.js';

// Event handlers
window.addEventListener('load', init);

// Init
function init() {
    console.log('initializing page...');
    getGeoLocation();
}

// Get geo location
function getGeoLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition((position) => {
            const coords = position.coords;
            console.log(coords);
            getLocationInfo(coords.latitude, coords.longitude);
        });
    }
}

// Get location info
function getLocationInfo(latitude, longitude) {
    getLocation(latitude, longitude).then((location) => {
        console.log(location);
        showLocationName(location);  // Show location name
        getLocationForecast(location.Key);  // Get location forecast
    });
}

// Show location name
function showLocationName(location) {
    document.getElementById('label-location').textContent =
        `${location.LocalizedName}, ${location.AdministrativeArea.LocalizedName}, ${location.Country.LocalizedName}`;
}

// Get forecast
function getLocationForecast(locationKey) {
    // Get weather forecast
    getWeather(locationKey).then((response) => {
        console.log(response);
        showForecast(response.DailyForecasts);
        updateChart(response.DailyForecasts);  // Llamar a updateChart aquí
    });
}

// Show daily forecast
function showForecast(daily) {
    console.log('Showing forecast data...');
    const forecastContainer = document.querySelectorAll('.day');
    daily.forEach((d, index) => {
        if (forecastContainer[index]) {
            const date = new Date(Date.parse(d.Date));
            forecastContainer[index].querySelector('.label-description').textContent = d.Day.IconPhrase;
            forecastContainer[index].querySelector('.label-temp-high').textContent = `${d.Temperature.Maximum.Value}°${d.Temperature.Maximum.Unit}`;
            forecastContainer[index].querySelector('.label-temp-low').textContent = `${d.Temperature.Minimum.Value}°${d.Temperature.Minimum.Unit}`;
            forecastContainer[index].querySelector('.label-name').textContent = getDayOfWeek(date);
            forecastContainer[index].querySelector('.label-date').textContent = formatDate(date);
        }
    });
}

// Update chart function
function updateChart(data) {
    for(var i = 0; i < data.length; i++) {
        var d = data[i];
        var date = new Date(Date.parse(d.Date));
        
        // Day of week
        document.getElementById('text-day' + (i + 1)).textContent = getDayOfWeek(date);
        
        // Temps
        document.getElementById('text-day' + (i + 1) + '-max').textContent = d.Temperature.Maximum.Value + '°' + d.Temperature.Maximum.Unit;
        document.getElementById('text-day' + (i + 1) + '-min').textContent = d.Temperature.Minimum.Value + '°' + d.Temperature.Minimum.Unit;
        
        // Resize column
        var maxHeight = (d.Temperature.Maximum.Value / 100) * 400;  // Escalar a la altura del gráfico
        var maxY = 450 - maxHeight;
        document.getElementById('day' + (i + 1) + '-max').setAttribute('height', maxHeight);
        document.getElementById('day' + (i + 1) + '-max').setAttribute('y', maxY);
        document.getElementById('text-day' + (i + 1) + '-max').setAttribute('y', maxY - 10);
        
        var minHeight = (d.Temperature.Minimum.Value / 100) * 400;  // Escalar a la altura del gráfico
        var minY = 450 - minHeight;
        document.getElementById('day' + (i + 1) + '-min').setAttribute('height', minHeight);
        document.getElementById('day' + (i + 1) + '-min').setAttribute('y', minY);
        document.getElementById('text-day' + (i + 1) + '-min').setAttribute('y', minY - 10);
    }
}
