import { config } from './config.js';

// Get location info
export async function getLocation(latitude, longitude) {
    console.log('Getting location info for ' + latitude + ',' + longitude);
    const requestUrl =
        `${config.url.api}locations/v1/cities/geoposition/search?apikey=${config.apiKey}&q=${latitude},${longitude}`;
    console.log(requestUrl);
    return await fetch(requestUrl)
        .then((result) => result.json())
        .catch((error) => { console.error(error); });
}

// Get 5 day forecast
export async function getWeather(locationId) {
    console.log('Getting weather for location ' + locationId);
    const requestUrl = `${config.url.api}forecasts/v1/daily/5day/${locationId}?apikey=${config.apiKey}`;
    console.log(requestUrl);
    return await fetch(requestUrl)
        .then((result) => result.json())
        .catch((error) => { console.error(error); });
}

// Get day of week
export function getDayOfWeek(date) {
    const days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    return days[date.getDay()];
}

// Format date
export function formatDate(date) {
    const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    return `${months[date.getMonth()]} ${date.getDate()}, ${date.getFullYear()}`;
}

// Get fontawesome icon
export function getIcon(index) {
    let icon = '';
    if (index <= 3) icon = 'sun';
    if (index >= 4 && index <= 5) icon = 'cloud-sun';
    if (index >= 7 && index <= 11) icon = 'cloud';
    if (index >= 12 && index <= 29) icon = 'cloud-rain';
    return icon;
}
